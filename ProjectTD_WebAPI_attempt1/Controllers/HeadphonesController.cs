﻿using ProjectTD_WebAPI_attempt1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectTD_WebAPI_attempt1.Controllers
{
    //This is the basic controller. it will return list of all headphones. It can also return a single headphone detail, based on the ID
    //The original tutorial used a generic product example. I decided to customize with a little more personal stuff. 
    public class HeadphonesController : ApiController
    {
        //as of now, I am not using any database. that demo will come back later. 
        //this solution will not be server deploy worthy even at the most basic level. thats because there is no database here. 
        //no database no serving. 

        //here, creating a collection of headphones we will run some queries on this array. 
        //Its quite possible that when you try to use the Headphone model, it wont pop up right away.
        //if so, go ahead and use the standard visual studio hover on red line trick and add the model to the current file.

        Headphone[] headphones = new Headphone[]
        {
            //here are some pointers
            //first up, the ID must be unique. 
            //and for the price, I am force to put the M at the end to indicate a literal decimal value. 

            new Headphone
            {
                Id = 1,
                Name = "Skull Candy Uproar",
                Category = "Wireless",
                Price = 4000.00M
            },
            new Headphone
            {
                Id = 2,
                Name = "Skull Candy Regular",
                Category = "Wired",
                Price = 3000.00M
            },
            new Headphone
            {
                Id = 3,
                Name = "Sony",
                Category = "Wired",
                Price = 400.00M
            }
        };

        //now that we have the array (data source) ready to kick ass, we need write some methods that will return the data
        //this the API system in action
        //we will be writing two API methods. The first will return all the headphones in our collection.
        //the second will return a specific headphone, based on the ID that is specified.

        //ther routes are configured in the file App_Start > WebApiConfig.cs

        //the following method will return all the products 
        //the route for this will be something like this - http://localhost:58356/api/headphones/showallheadphones

        [System.Web.Http.AcceptVerbs("GET", "POST")]  //this line and the next line
        [System.Web.Http.HttpGet]//are added to allow this method to be liked a standard API call. 
        [ActionName("showallheadphones")]
        public IEnumerable<Headphone> ReturnAllHeadphones()
        {
            //I am return the entire array here. 
            //notice the IEnumerable in the method definition? Yeah, that will take care of convering this array into a collection
            //besides, the array is itself treated as a collection, so its alright.
            return headphones;
        }

        //the following method will return one specific head phone
        //ther route for this will be something like this - http://localhost:58356/api/headphones/showspecificheadphone/2
        [System.Web.Http.AcceptVerbs("GET", "POST")]  //this line and the next line
        [System.Web.Http.HttpGet]//are added to allow this method to be liked a standard API call. 
        [ActionName("showspecificheadphone")]
        public IHttpActionResult ReturnHeadphone(int id)
        {
            //run a query to return the specific headphone we are looking for.
            var head_phone = headphones.FirstOrDefault((p) => p.Id == id);

            //if no product was found, then we must return a value that indicates that nothing was found
            if(head_phone == null)
            {
                return NotFound();
            }

            //we will reach this point only if an actual headphone with the ID was found. 
            return Ok(head_phone);
        }

        //I am going to try one more method. 
        //the route for this will be something like this - http://localhost:58356/api/headphones/showsheadphonesasstring
        [System.Web.Http.AcceptVerbs("GET", "POST")]  //this line and the next line
        [System.Web.Http.HttpGet]//are added to allow this method to be liked a standard API call. 
        [ActionName("showsheadphonesasstring")]
        public string returnheadphonesstring()
        {
            string headphone_list = ""; //creating an empty string

            //looping through each item in the collection, and adding it to the string declared above
            foreach (var x in headphones)
            {
                //converting the current object into some adhoc string
                string temp = "serial number - " + x.Id.ToString() + " Name -  " + x.Name.ToString() + " Category - " + x.Category.ToString() + " Price - " + x.Price.ToString() + " \n";
                //adding the string to the main string
                headphone_list = temp + headphone_list;
            }

            //returning the full string
            return headphone_list;
        }

    }
}
