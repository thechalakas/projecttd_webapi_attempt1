﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTD_WebAPI_attempt1.Models
{
    //this class will hold details about some head phone brands
    //I decided to go with headphones because I just bought these Skull Candy Wireless Headphones. 
    //They are simply amazing.
    public class Headphone
    {
        //this is the primary key stuff. 
        public int Id { get; set; }
        //this is the headphones name. right now there are no limitations.
        public string Name { get; set; }
        //category like, wireless or not wireless
        public string Category { get; set; }
        //price stuff. Like 100.10 rupees. 
        public decimal Price { get; set; }
    }
}